/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Sandra
 */
public class Aula {
    private String codi;
    private String nom;

    
     public Aula(String codigo, String nombre) {
        this.codi = codigo;
        this.nom = nombre;
    }

    @Override
    public String toString() {
        return "Aula{" + "codi=" + codi + ", nom=" + nom + '}';
    }
    
    
    
    
    
    public String getCodi() {
        return codi;
    }

    public void setCodi(String codi) {
        this.codi = codi;
    }

   

    public Aula() {
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
 
}
