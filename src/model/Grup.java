/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Set;

/**
 *
 * @author Sandra
 */
public class Grup implements Serializable {
    private String codi;
    private Nivell nivell;
    //este array no el afegim al constructor perque el anyadirem després en un set
    private Set<Alumne> llistaAlumnes;
    private Alumne delegat;
    private Aula a;

    
    public Grup() {
    }

    public Grup(String codi, Nivell nivell) {
        this.codi = codi;
        this.nivell = nivell;
    }

    @Override
    public String toString() {
        return "Grup{" + "codi=" + codi + ", nivell=" + nivell + ", delegat=" + delegat.getNexp() + ", a=" + a + '}';
    }

   
    public void imprimirLlistatAlumnes(){
        for (Alumne a : llistaAlumnes) {
            System.out.println(a);
        }
    }

    public Aula getA() {
        return a;
    }

    public void setA(Aula a) {
        this.a = a;
    }
    
    public String getCodi() {
        return codi;
    }

    public void setCodi(String codi) {
        this.codi = codi;
    }

    public Nivell getNivell() {
        return nivell;
    }

    public void setNivell(Nivell nivell) {
        this.nivell = nivell;
    }

    public Alumne getDelegat() {
        return delegat;
    }

    public void setDelegat(Alumne delegat) {
        this.delegat = delegat;
    }

    public Set<Alumne> getLlistaAlumnes() {
        return llistaAlumnes;
    }

    public void setLlistaAlumnes(Set<Alumne> llistaAlumnes) {
        this.llistaAlumnes = llistaAlumnes;
    }
    
    
   
}
