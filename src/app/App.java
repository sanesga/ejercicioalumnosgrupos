/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import model.Alumne;
import model.Aula;
import model.Grup;
import model.Nivell;
import model.Nom;
import model.Sexe;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author Maite
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);

        int opcion;
        Alumne a = null;
        Calendar dataNaix = Calendar.getInstance();
        Grup grup;
        Set<Alumne> llistaAlumnes = new HashSet<>();
        Grup g = null;
        Nivell n = null;
        Sexe s = null;
        Nom nom;
        Query query;
        Aula aula;

        //CREAMOS CONEXION
        SessionFactory sessionFactory;
        Configuration configuration = new Configuration();
        configuration.configure();
        sessionFactory = configuration.buildSessionFactory();
        SessionFactory factory = new Configuration().configure().buildSessionFactory();

        //CREAR UNA SESION
        Session session = factory.openSession();

        do {
            System.out.println();
            System.out.println("Elige opción");
            menu();
            opcion = teclado.nextInt();

            switch (opcion) {
                case 1://CREAR GRUP

                    dataNaix.set(1992, 03, 22);
                    nom = new Nom("Juan");
                    g = new Grup("C2", n.BATXILLER);
                    a = new Alumne(nom, 954473727, s.MASCULI, dataNaix.getTime(), 1, g);
                    llistaAlumnes.add(a);
                    g.setLlistaAlumnes(llistaAlumnes);
                    //triem qui serà el delegat i afegim el numero d'expedients al camp delegat de grup
                    g.setDelegat(a);
                    //creem el aula i li fem el set a grup
                    aula = new Aula("B1A", "Aula d'informàtica");
                    g.setA(aula);
                    System.out.println("Grup creat amb éxit");
                    break;
                case 2://GUARDAR GRUP
                    session.beginTransaction();
                    session.save(g);
                    session.getTransaction().commit();
                    System.out.println("Grup guardat amb èxit");
                    break;
                case 3://LLEGIR GRUP
                    g = session.get(Grup.class, "B1A");
                    System.out.println(g);
                    g.imprimirLlistatAlumnes();
                    break;
                case 4://ACTUALITZAR GRUP
                    session.beginTransaction();
                    g.setNivell(n.CF);
                    session.update(g);
                    session.getTransaction().commit();
                    System.out.println("Grup actualitzat amb èxit");
                    break;
                case 5://EL.LIMINAR GRUP
                    session.beginTransaction();
                    session.delete(g);
                    session.getTransaction().commit();
                    System.out.println("Grup el.liminat amb èxit");
                    break;
                case 6://CREEM ALUMNE I GUARDEM
                    session.beginTransaction();

                    //OBTENIM EL GRUP I EL ARRAY ON VOLEM ANYADIR EL ALUMNE
                    g = session.get(Grup.class, "B1A");
                    llistaAlumnes = g.getLlistaAlumnes();

                    //CREEM EL ALUMNE
                    dataNaix.set(1996, 11, 30);
                    nom = new Nom("María");
                    a = new Alumne(nom, 961234567, s.FEMENI, dataNaix.getTime(), 2, g);

                    //ANYADIM EL ALUMNE AL ARRAY I ACTUALITZEM EL GRUP
                    llistaAlumnes.add(a);
                    session.update(g);

                    session.getTransaction().commit();
                    System.out.println("Alumne creat amb èxit");
                    break;
                case 7://LLEGIR ALUMNE
                    a = session.get(Alumne.class, 1);
                    System.out.println(a);

                    break;
                case 8://ACTUALITZAR ALUMNE
                    session.beginTransaction();
                    nom = new Nom("Benito");
                    a.setNom(nom);
                    session.getTransaction().commit();
                    System.out.println("Alumne actualitzat amb èxit");
                    break;
                case 9://EL.LIMINAR ALUMNE
                    session.beginTransaction();
                    g = session.get(Grup.class, "B1A");
                    llistaAlumnes = g.getLlistaAlumnes();
                    llistaAlumnes.remove(a);
                    g.setLlistaAlumnes(llistaAlumnes);
                    session.update(g);
                    session.getTransaction().commit();
                    System.out.println("Alumne eliminado con éxito");
                    break;
                case 10://CONSULTA 1. Seleccionar el nombre de los alumnos que son hombres y mayores de 18 años
                    query = session.createQuery("SELECT a.nom FROM Alumne a WHERE sexe=1 AND datanaix > '2001-01-01'");
                    List<Object> listDatos = query.list();
                    for (Object dato : listDatos) {
                        System.out.println(dato);
                    }
                    break;
                case 11://Seleccionar el nombre de los alumnos que han suspendido las mismas asignaturas que aquellos cuyo apellido empieza por F
                    query = session.createQuery("SELECT a.nom FROM Alumne a WHERE a.susp = (SELECT a.susp FROM Alumne a WHERE a.nom LIKE 'J%') ");
                    listDatos = query.list();
                    for (Object dato : listDatos) {
                        System.out.println(dato);
                    }
                    break;
                case 12://Selecciona nexp y nombre de los alumnos y nombre del grupo en que son delegados"
                    query = session.createQuery("SELECT a.nexp, a.nom, g.codi FROM Alumne a, Grup g WHERE a.grup = g.codi");
                    List<Object[]> listaDatos = query.list();
                    for (Object[] dato : listaDatos) {
                        System.out.println(dato[0] + "--" + dato[1] + "--" + dato[2]);
                    }
                    break;
                case 13://Selecciona alumnos que han suspendido más que la media
                    query = session.createQuery("SELECT a FROM Alumne a WHERE a.susp > (SELECT AVG(a.susp) FROM Alumne a)");
                    List<Alumne> alumnes = query.list();
                    for (Alumne al : alumnes) {
                        System.out.println(al);
                    }
                    break;
                case 14://Selecciona el nombre de los alumnos que han suspendido más de 2 asignaturas
                    query = session.createQuery("SELECT a.nom FROM Alumne a WHERE a.susp > 2");
                    List<Object> nombres = query.list();
                    for (Object dato : nombres) {
                        System.out.println(dato);
                    }
                    break;
                case 15://Selecciona los grupos y el número de alumnos matriculados en cada uno"
                    query = session.createQuery("SELECT g, COUNT(a) FROM Grup g , Alumne a WHERE a.grup = g.codi");
                    List<Object[]> lista = query.list();
                    for (Object[] l : lista) {
                        System.out.println(l[0] + "--" + l[1]);
                    }
                    break;
                case 16://Selecciona los datos de los alumnos por fecha de nacimiento --CON PARÁMETROS POR POSICION--
                    String fecha = "1992-04-22";
                    query = session.createQuery("SELECT a FROM Alumne a WHERE dataNaix=?0");
                    query.setString(0, fecha);

                    List<Alumne> alumnes2 = query.list();
                    for (Alumne al : alumnes2) {
                        System.out.println(al);
                    }
                    break;
                case 17://Selecciona los datos de los alumnos por fecha de nacimiento --CON PARÁMETROS POR NOMBRE--"
                    String dataNaix2 = "1992-04-22";

                    query = session.createQuery("SELECT a FROM Alumne a WHERE dataNaix=:dataNaix");
                    query.setString("dataNaix", dataNaix2);

                    List<Alumne> alumnes3 = query.list();
                    for (Alumne al : alumnes3) {
                        System.out.println("Alumne " + al);
                    }
                    break;
                case 18://Realizar la consulta anterior pero en el fichero de mapeo llamándola desde el main
                    query = session.getNamedQuery("ObtenerAlumnosPorFechaNacimiento");
                    String dataNaix3 = "1992-04-22";

                    query.setParameter("dataNaix", dataNaix3);

                    List<Alumne> alumnes4 = query.list();

                    for (Alumne al : alumnes4) {
                        System.out.println("Alumne " + al);
                    }
                    break;
                case 20://EIXIR
                    session.close();
                    sessionFactory.close();
                    System.out.println("Saliendo..");
                    break;
                default:
                    throw new AssertionError();
            }

        } while (opcion
                != 20);

    }

    public static void menu() {

        System.out.println("CONSIDERACIÓN 1. EJECUTAR LOS PASOS POR ORDEN (1º CREAR Y 2º GUARDAR)");
        System.out.println("CONSIDERACIÓN 2. RECORDAR SALIR DE LA APLICACIÓN PARA CERRAR SESIÓN");
        System.out.println("CONSIDERACIÓN 3. SI SE INTENTA VOLVER A AÑADIR ALUMNE, COMO SON FIJOS DARÁ ERROR");

        System.out.println("1. Crear grup");
        System.out.println("2. Guardar grup");
        System.out.println("3. Llegir grup");
        System.out.println("4. Actualitzar grup");
        System.out.println("5. El.liminar grup");

        System.out.println("6. Crear i guardar alumne");
        System.out.println("7. Llegir alumne");
        System.out.println("8. Actualitzar alumne");
        System.out.println("9. El.liminar alumne");

        System.out.println("-----------CONSULTAS-------------");
        System.out.println("10. Seleccionar el nombre de los alumnos que son hombre y mayores de 18 años");
        System.out.println("11. Seleccionar el nombre de los alumnos que han suspendido las mismas asignaturas que aquellos cuyo apellido empieza por F");
        System.out.println("12. Selecciona nexp y nombre de los alumnos y nombre del grupo en que son delegados");
        System.out.println("13. Selecciona alumnos que han suspendido más que la media");
        System.out.println("14. Selecciona el nombre de los alumnos que han suspendido más de 2 asignaturas");
        System.out.println("15. Selecciona los grupos y el número de alumnos matriculados en cada uno");
        System.out.println("16. Selecciona los datos de los alumnos por fecha de nacimiento --CON PARÁMETROS POR POSICION-- DA ERROR DEPRECATED!!!");
        System.out.println("17. Selecciona los datos de los alumnos por fecha de nacimiento --CON PARÁMETROS POR NOMBRE--");
        System.out.println("18. Realizar la consulta anterior pero en el fichero de mapeo llamándola desde el main");

        System.out.println("20. Salir");
    }
}
